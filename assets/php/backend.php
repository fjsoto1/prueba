<?php
    // Autor: Noe Misael Montoya Ibarra
    // Fecha: 03/Enero/2022
    // Descripcion: Creacion de CRUD CON EXT  
    /*-- Datos de Conexion a Servidor MySQL local --*/
    require('confighost.php');   
    $conn = mysqli_connect($servername, $username, $password, $database);
    //TEST DE CONEXION
    if (!$conn) 
    {
        die("Connection failed: " . mysqli_connect_error());
    }
    
    //echo "Connected successfully";  <-- Verficacion de conexion con servidor

    /* Diccionario de Funciones
     - CREATE: postRecord()         <-- Crea un nuevo registro en una tabla (INSERT INTO nameTable VALUES (null,'Misael',23,6621074722))
     - READ:   getRecords()         <-- Extrae todos los registros de una tabla (SELECT * FROM nameTable)
     - READ:   getRecordSearch()    <-- Extrae los registros que cumplan con los parametrosdebusqueda
     - READ:   getRecord()          <-- Extrae un registro especifico de una tabla (SELECT * FROM nameTable WHERE id='2')
     - UPDATE: putRecord()          <-- Actualiza un registro en una tabla (UPDATE nameTable SET... )
     - DELETE: deleteRecord()       <-- Elimina un registro en una tabla (DELETE fisico o logico traves de estado)    
    */

    $funcionExecute = $_POST["funcionEjecutar"]; 

    switch ($funcionExecute) {
        case 1: 
            getRecords();   
            break;
        case 2:
            getRecordSearch();
            break;
        case 3:
            getRecord();
            break;
        
        case 4:
            postRecord();
            break;

        case 5:
            putRecord();
            break;

        case 6:
            deleteRecord();
            break;
    }

    function postRecord()
    { 
        global $conn; //<--haciendo la variable de conexion global, para poder acceder a ella desde una funcion
        $namePerson=$_POST["nameCustomer"];
        $genderPerson = $_POST["genderCustomer"];
        $phoneNumberPerson=$_POST["numberPhoneCustomer"];
        $bornDateCustomer=$_POST["bornDateCustomer"];
        $createRow="INSERT INTO clientes VALUES (null,'$namePerson','$genderPerson','$phoneNumberPerson','$bornDateCustomer');"; 
        if (mysqli_query($conn, $createRow)) 
        {
            $response = array( "success"=>"true", "msg"=>"Insercion Exitosa!!");
            echo json_encode($response);
        }
        else
        {
            //echo "Error: " . $createRow . "<br>" . mysqli_error($conn);
            $response = array( "error"=>"true", "msg"=>"Fallo la insercion: ", "error"=> mysqli_error($conn));
            echo json_encode($response);
        }
    }

    function getRecords()
    {
        global $conn;
        
        $query = "SELECT id,nombre,sexo,telefono,DATE_FORMAT(nacimiento,'%d-%m-%Y') as nacimiento FROM clientes ORDER BY id DESC"; // Consulta SQL
        $result = mysqli_query($conn, $query);
            
            //Preparamos la respuesta, generaremos un JSON como lo Solicta EXT
        $data = array();  // Creamos un arreglo que almacenara la respuesta de la consulta
        while($row =mysqli_fetch_assoc($result)) // Recorremos los resultados de la consulta            
        {
            $data[] = $row;
        }
        //Empaquetamos toda la informacion para en un nuevo arreglo para generar la estructura de JSON en 
        //texto plano y lo retornamos como un JSON usando json_encode
        $response = array( "success"=>"true", "data"=>$data );
        echo json_encode($response);
    }

    function getRecordSearch()
    {
        global $conn;      
        $namePerson = $_POST["word"];
        //Consulta con LIKE
        $query = "SELECT id,nombre,sexo,telefono,DATE_FORMAT(nacimiento,'%d-%m-%Y') as nacimiento FROM clientes WHERE nombre LIKE '%".$namePerson."%'   "; // Consulta SQL
        $result = mysqli_query($conn, $query);
        //Preparamos la respuesta, generaremos un JSON como lo Solicta EXT
        $data = array();  // Creamos un arreglo que almacenara la respuesta de la consulta
        while($row =mysqli_fetch_assoc($result)) // Recorremos los resultados de la consulta
        {
            $data[] = $row;
        }

        //Empaquetamos toda la informacion para en un nuevo arreglo para generar la estructura de JSON en 
        //texto plano y lo retornamos como un JSON usando json_encode
        $response = array( "success"=>"true", "data"=>$data );
        echo json_encode($response);
    }

    //-------[ Trabajando ] -------//
    function getRecord()
    {
        global $conn;
        $id = $_POST["id"];
        //Consulta con LIKE
        $query = "SELECT * FROM clientes WHERE id = '$id'"; // Consulta SQL
        $result = mysqli_query($conn, $query);        
        //Preparamos la respuesta, generaremos un JSON como lo Solicita EXT
        $data = array();  // Creamos un arreglo que almacenara la respuesta de la consulta
        while($row =mysqli_fetch_assoc($result)) // Recorremos los resultados de la consulta
        {
            $data[] = $row;
        }
        //Empaquetamos toda la informacion para en un nuevo arreglo para generar la estructura de JSON en 
        //texto plano y lo retornamos como un JSON usando json_encode
        $response = array( "success"=>"true", "data"=>$data );
        echo json_encode($response);
    }


    //-------[ No trabajadas ] -------//
    
    function putRecord()
    {
       
       global $conn;      
       $id = $_POST["id"];
       $namePerson=$_POST["nameCustomer"];
       $genderPerson = $_POST["genderCustomer"];
       $phoneNumberPerson=$_POST["numberPhoneCustomer"];
       $bornDateCustomer=$_POST["bornDateCustomer"];
        //Consulta con LIKE
       $query = "UPDATE clientes SET nombre ='$namePerson',sexo ='$genderPerson', telefono ='$phoneNumberPerson', nacimiento='$bornDateCustomer' WHERE id= '$id'"; // Consulta SQL
       if (mysqli_query($conn, $query)) 
       {
           $response = array( "success"=>"true", "msg"=>"Acctualizacion Exitosa!!");
           echo json_encode($response);
       }
       else
       {
           $response = array( "error"=>"true", "msg"=>"Fallo la actulizacion: ", "error"=> mysqli_error($conn));
           echo json_encode($response);
       }
        
    }

    function deleteRecord()
    {
       global $conn;      
       $id = $_POST["id"];
        //Consulta con LIKE
       $query = "DELETE FROM clientes WHERE id = '$id'"; // Consulta SQL
       $result = mysqli_query($conn, $query);
        
       if (mysqli_query($conn, $result)) 
       {
           $response = array( "success"=>"true", "msg"=>"Eliminacion Exitosa!!");
           echo json_encode($response);
       }
       else
       {
           //echo "Error: " . $createRow . "<br>" . mysqli_error($conn);
           $response = array( "error"=>"true", "msg"=>"Fallo la Eliminacion: ", "error"=> mysqli_error($conn));
           echo json_encode($response);
       }
    }
 


  

    mysqli_close($conn);

?>