// █ █▄ █ ▀█▀ █▀█ █▀█ █▀▄ █ █ █▀▀ █▀▀ █ █▀█ █▄ █   █▀█ ▄▀█ █▀█ █ █▀▄ ▄▀█   ▄▀█   █▀▀ ▀▄▀ ▀█▀
// █ █ ▀█  █  █▀▄ █▄█ █▄▀ █▄█ █▄▄ █▄▄ █ █▄█ █ ▀█   █▀▄ █▀█ █▀▀ █ █▄▀ █▀█   █▀█   ██▄ █ █  █ 
//
// Autor: Noe Misael Montoya Ibarra
// Fecha: 08/Enero/2022

//-----------------------------------------------------------------
// ▄█     █▀▄▀█ █▀█ █▀▄ █▀▀ █   █▀  ▄▀ █▀▄▀█ █▀█ █▀▄ █▀▀ █   █▀█ █▀
// ░█ ▄   █░▀░█ █▄█ █▄▀ ██▄ █▄▄ ▄█ ▄▀  █░▀░█ █▄█ █▄▀ ██▄ █▄▄ █▄█ ▄█

/* Concepto:
*   Un modelo es una representación de la información en nuestro dominio de aplicación. Cada modelo representa una entidad como 
*   Usuario, Factura, Cliente, o lo que necesitemos en nuestro proyecto. Un modelo puede contener campos, validaciones y relaciones 
*   entre otros modelos.También podemos configurar un proxy para que persista y extraiga nuestros datos.
*/

/*
//Paso 1. Definir el modelo de datos con el que trabajara
Ext.define('MyApp.model.Customer', {
    extend: 'Ext.data.Model',
    idProperty: 'idCustomer',// Definimos la propiedad en nuestra respuesta JSON que contendrá el ID de cada instancia de registro
    fields: [
        //Definimos los campos del modelo, con algunas propiedades
        { name: 'id' },
        { name: 'namePerson' },
        { name: 'phoneNumber' },
        { name: 'bornPerson', type: 'date', dateFormat: 'Y-m-d' }
    ],
    //Definimos las validaciones para los campos.
    validations: [
        //presence === required
        //length === longitud
        { type: 'presence', field: 'namePerson' },
        { type: 'length', field: 'phoneNumber', min: 10, max: 10 }
    ]
});

//Paso 2. Probar el modelo de datos 
var customer = Ext.create("MyApp.model.Customer", {
    id: '12345',
    namePerson: 'Noe Misael Montoya Ibarra',
    phoneNumber: '66 21 07 47 22',
    bornPerson: '1997-02-10'
});
*/
//Mostrando data general
//console.log(customer);

/* Mostrando data especifica del modelo
    var nameP = customer.get('namePerson'); console.log(nameP);
    var phoneN = customer.get('phoneNumber'); console.log(phoneN);
    var bornP = customer.get('bornPerson'); console.log(bornP);
*/

/*Modificacion de datos guardados en el modelo
    var name = customer.get('namePerson') //Extraemos el nombre del cliente alamacenado en el modelo
    customer.set('namePerson', 'Mr. Smith');// Lo reasignamos en el modelo
    console.log('Old name:', name); //Mostramos el antiguo nombre del cliente previamente guardado en una variable
    console.log('New name:', customer.get('namePerson')); // Mostramos el nuevo nombre del cliente almacenado en el modelo
*/

//Ejecucion de validaciones

//Personalizacion de mensajes en las validaciones.
//Es posible agregar mas reglas, ver Capitulo 4 de libro, subtitulo Validations
/*
Ext.data.validations.lengthMessage = '- Es requerido/No tiene la longitud permitida...';
Ext.data.validations.presenceMessage = '- Es requerido...';

var customer1 = Ext.create("MyApp.model.Customer", {
    id: '12345',
    namePerson: 'Noe Misael Montoya',
    phoneNumber: '6621074722',
    bornPerson: '1997-02-10'
});

if (customer1.isValid()) { //Ejecucion de las reglas de validacion
    console.log('Data correcta!'); // Si esta OK
} else {
    var errors = customer1.validate(); //Obtiene los errores para que sean mostrados en consola.
    errors.each(function (error) {


        console.log(error.field, error.message);
    });
}
*/

//------------------------------------------------------------
// ▀█     █▀ ▀█▀ █▀█ █▀█ █▀▀    ▄▀   ▀█▀ █ █▀▀ █▄░█ █▀▄ ▄▀█ █▀
// █▄ ▄   ▄█ ░█░ █▄█ █▀▄ ██▄   ▄▀    ░█░ █ ██▄ █░▀█ █▄▀ █▀█ ▄█

/* Concepto:
    Como se mencionó anteriormente, una tienda es una colección de modelos que actúa como un caché de cliente para administrar 
    nuestros datos localmente.También podemos extraer datos de nuestro servidor utilizando uno de los proxies disponibles y un 
    lector para interpretar la respuesta del servidor y completar la colección.
*/

/*
//Paso 1. Definir el Store y asociarlo con el modelo con el que trabajara
Ext.define('MyApp.store.Customers', {
    extend: 'Ext.data.Store',
    model: 'MyApp.model.Customer'
});

//Paso 2. Instanciar el store
var store = Ext.create("MyApp.store.Customers");


//Paso 3. Añadir elementos al Store...
// Se utilizaran las funciones de agregar o insertar del Store.
//Opcion 1
// Paso 3.1 Crear un modelo con datos
var s_customer1 = Ext.create("MyApp.model.Customer", {
    id: '1',
    namePerson: 'Noe Montoya',
    phoneNumber: '6621074722',
    bornPerson: '1997-02-10'
});
var s_customer2 = Ext.create("MyApp.model.Customer", {
    id: '2',
    namePerson: 'Yamileth Ybarra',
    phoneNumber: '6621074723',
    bornPerson: '2000-02-10'
});
var s_customer3 = Ext.create("MyApp.model.Customer", {
    id: '3',
    namePerson: 'Misael Robles',
    phoneNumber: '6621074723',
    bornPerson: '2000-02-10'
});
var s_customer4 = Ext.create("MyApp.model.Customer", {
    id: '4',
    namePerson: 'Nayely Ybarra',
    phoneNumber: '6621074723',
    bornPerson: '2000-02-10'
});

//Paso 3.2 Ejecutar la funcion add
store.add(s_customer1, s_customer2, s_customer3, s_customer4);

//Probar que haya funcionado el add del Store
console.log(store.count()); // Cuenta los elementos en el Store
*/

/* Opcion 2
    store.add({
        id: '2',
        namePerson: 'Nayely Ybarra',
        phoneNumber: '6621074722',
        bornPerson: '2000-02-10'
    });
*/

/*
// Añadir elementos en el STORE por posicion
    store.insert(0, // Definies la posicion en la que era insertado
    { // Defines la data del Modelo 
        taxId: "L125AP",
        name: "John Smith",
        address: "159 ST, TX USA",
        creditCard: "1234567890123456"
    });
*/

//---- Recorrido de Elementos de una Store ----//
/*
    store.each(function (record, index) {
        console.log(index, record.get("namePerson","phoneNumber"));
    });
*/
/*
//Definiendo un rango de recorrido.
var list = store.getRange(0, 3);
Ext.each(list, function (record, index) {
    console.log(index, record.get("namePerson"));
});

//Estrayendo el primero y el Ultimo
var first = store.first(), last = store.last();
console.log(first.get("namePerson"), last.get("namePerson"));

//Eliminado modelos del Store
store.remove(s_customer2); // Por nombre del modelo
store.each(function (record, index) {
    console.log(index, record.get("namePerson"));
});
*/
/*Eliminar Por posicion en el store Primero y Ultimo
    store.remove([first,last]); 
*/

/*Eliminar un registro por su posición en la tienda, como se muestra en el siguiente código:
    store.removeAt(2);
    store.each(function (record, index) {
        console.log(index, record.get("name"));
    })
*/

//Si queremos eliminar todos los registros de nuestra tienda, solo necesitamos llamar a removeAll método y la tienda se despejará.
// store.removeAll(); console.log("Records:", store.count());

//-------------------------------------------------
// █▀▀ █▀█ █▄░█ █▀▀ ▀▄▀ █ █▀█ █▄░█   ▄▀█   █▀▄ █▄▄
// █▄▄ █▄█ █░▀█ ██▄ █░█ █ █▄█ █░▀█   █▀█   █▄▀ █▄█
/*
//Paso 1. Definir las propiedades de conexion a modelos y store que manejara las peticiones ajax
Ext.define('MyApp.store.Customers', {
    extend: 'Ext.data.Store',
    model: 'MyApp.model.customer',
    proxy: {
        type: 'ajax',
        url: '../php/backend.php/' //URL del sitio e indicar que retorna los datos por JSON
    },
    reader: { //Establecemos las propiedades de lectura para JSON
        type: 'json',
        root: 'data'
    }
});

//Paso 2 Definir la nueva tienda que contendra los datos extraidos de la DB
var store = Ext.create("MyApp.store.Customers"); //Step 1
store.load(function () { //Step 2
    store.each(function (record) { //Step 3
        console.log(record.get("namePerson"));
    });
});
*/

//Notas:
/*Step 2: Este método ejecuta internamente la operación de lectura, realiza la llamada Ajax al servidor y  luego carga los datos en la tienda. 
    La función que le damos alcarga El método como parámetro es una devolución de llamada que se ejecutará después de que los registros se carguen 
    en la tienda. Lo estamos haciendo de esta manera porque Ajax es asincrónico y no sabemos con certeza cuándo responderá el servidor.
 *Step 3: El último paso recorre en iteración los registros de la tienda e imprime el nombre de cada factura en la consola de JavaScript.
*/























