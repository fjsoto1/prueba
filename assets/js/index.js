// Autor: Noe Misael Montoya Ibarra
// Fecha: 08/Enero/2022

// Aqui se puede colocar referencias y funciones que seran utilizadas en el fronted de Ext...

//▀█▀ █░█  █▀▀ █▀█ █▀▄ █ █▀▀ █▀█ ▄▀█ █▀█ █░█ █
// █░ █▄█  █▄▄ █▄█ █▄▀ █ █▄█ █▄█ █▀█ ▀▀█ █▄█ █

/* Pasos Seguidos para crear este ejemplo
    1. Se declara el evento que renderiza los formularios y demas objetos de Ext
    2. Crea un panel, donde se mostraran los objetos, por ejemplo, inputs,botones,grids, etc.
    3. Crea los modelos que seran utilizados para mostrar los datos, con sus respectivos Stores. 
        Nota: Estos se crean antes de que exista el panel, recuerda que EXT esta basado en MVC.
    4. Crea el Store que gestionara la informacion de la base de datos, configura ID, Modelo que pertenece y demas opciones
    5.Que vuele la imgainacion

*/

// Definicion de modelo
//https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.data.Model 
// El modelo se declara tal cual como viene en la base de datos, respeta las mayusculas y minusculas
Ext.define('CustomerModel', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id' },
        { name: 'nombre' },
        { name: 'sexo' },
        { name: 'telefono' },
        { name: 'nacimiento' }
    ]
});

//Definicion de Store
//https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.data.Store
var CustomerStore = Ext.create('Ext.data.Store',
    {

        //Indicaremos que podra realizar peticiones AJAX
        //https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.data.proxy.Ajax
        storeId: 'CustomerStore', // ID del Store
        model: 'CustomerModel',  // Asosiamos al modelo que hemos definido 
        proxy:
        {
            url: 'assets/php/backend.php', // Esta va de acuerdo a nivel de los archivos
            type: 'ajax',
            actionMethods:
            {
                create: "POST",
                read: "POST",
                update: "POST",
                destroy: "POST"
            },
            reader:
            {
                type: 'json',
                root: 'data' // Acceder a sub-niveles en el JSON de respuesta
            }
        }
    });

//Store para los generos SELECT para genero
var gender = Ext.create('Ext.data.Store', {
    fields: ['key', 'label'],
    data: [{ key: 'M', label: 'Hombre/Masculino' }, { key: 'F', label: 'Mujer/Femenino' }, { key: 'NB', label: 'No Binario' }]
});

//Declaramos una Window, es el equivalente a un modal tradicional
//https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.window.Window
//Al usar una plantilla default de textfield es necesario implementarla de esta manera
function openCustomerModal() {
    var modal = Ext.create('Ext.window.Window', {
        title: 'Nuevo Cliente',
        width: 400,
        height: 300,
        modal: true,
        layout: 'fit',
        items:
        {
            //https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.layout.container.Form
            items: [
                {
                    layout: 'form',
                    width: 390,
                    height: 260,
                    bodyPadding: 25,
                    defaultType: 'textfield',
                    items: [
                        {
                            //Textbox (Input Text)
                            fieldLabel: 'Nombre:',
                            id: 'name_customer', //asigna un id asi como con JQUERY
                            name: 'name',
                            style: "margin-top: 10px !important;",
                            allowBlank: false
                        },
                        {
                            //Combobox (Select)
                            fieldLabel: 'Genero',
                            xtype: 'combobox',
                            id: 'gender_customer',
                            store: gender,
                            queryMode: 'local',
                            valueField: 'key',
                            // Para poder rendirizar los valores del store en el input SELECT 
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item">{key} - {label}</div>',
                                '</tpl>'
                            ),
                            // template for the content inside text field
                            displayTpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">', '{key} - {label}', '</tpl>')
                        },
                        {
                            fieldLabel: 'Telefono:',
                            id: 'numberPhone',
                            name: 'numberPhone',
                            style: "margin-top: 10px !important;",
                            allowBlank: false,
                            // items: [text_search]

                        },
                        {
                            //Datetimepicker (Date)
                            xtype: 'datefield',
                            fieldLabel: 'Nacimiento:',
                            id: 'bornDate_customer',
                            name: 'bornDate',
                            style: "margin-top: 10px !important;",
                            format: 'Y/m/d',
                            submitFormat: 'Y-m-dH:m:s',
                            allowBlank: false
                        },
                    ],
                    buttons:
                        [
                            {
                                text: 'Cancelar',
                                bodyPadding: 25,
                                iconCls: 'icon-cancel',
                                handler: function () {

                                    //Limpiado el formulario de manera manual
                                    Ext.getCmp('name_customer').setValue('');
                                    Ext.getCmp('gender_customer').setValue('');
                                    Ext.getCmp('numberPhone').setValue('');
                                    Ext.getCmp('bornDate_customer').setValue('');
                                    modal.close();
                                }
                            },
                            {
                                text: 'Guardar',
                                iconCls: 'icon-save',
                                formBind: true, //only enabled once the form is valid
                                disabled: false,
                                handler: function () {
                                    //Recuperamos los valores de los inputsy los asignamos a variables
                                    let name_customer = Ext.getCmp('name_customer').getValue(); // Rucupera valor por id
                                    //Validamos que no este vacio el input
                                    if (name_customer == "") {
                                        //https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.window.MessageBox
                                        Ext.Msg.show({
                                            title: 'Atención.',
                                            msg: 'Ingresa el nombre del cliente, es requerido.',
                                            buttons: Ext.Msg.OK,// OK - NO - YES - OKCANCEL - YESNO - YESNOCANCEL
                                            icon: Ext.Msg.WARNING, //INFO - WARNING - QUESTION - ERROR
                                            width: 350, height: 130
                                        });
                                        Ext.getCmp('name_customer').focus(); // Devolvemos el foco al campo.
                                        return;
                                    }

                                    let gender_customer = Ext.getCmp('gender_customer').getValue();
                                    if (gender_customer == "" || gender_customer == null) {
                                        Ext.Msg.show({
                                            title: 'Atención.',
                                            msg: 'Selecciona el genero del cliente, es requerido.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.WARNING,
                                            width: 350, height: 130
                                        });
                                        Ext.getCmp('gender_customer').focus();
                                        return;
                                    }

                                    let numberPhone_customer = Ext.getCmp('numberPhone').getValue();
                                    if (numberPhone_customer == "") {
                                        Ext.Msg.show({
                                            title: 'Atención.',
                                            msg: 'Ingresa el no. de telefonico del cliente, es requerido.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.WARNING,
                                            width: 350, height: 130
                                        });
                                        Ext.getCmp('numberPhone').focus();
                                        return;
                                    }

                                    let bornDate_customer = Ext.Date.format(Ext.getCmp('bornDate_customer').getValue(), 'Y-m-d'); // Tratamiento a input DATE
                                    if (bornDate_customer == "") {
                                        Ext.Msg.show({
                                            title: 'Atención.',
                                            msg: 'Selecciona la fecha de nacimiento del cliente, es requerido.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.WARNING,
                                            width: 350, height: 130
                                        });
                                        Ext.getCmp('bornDate_customer').focus();
                                        return;
                                    }
                                   // console.log(name_customer, gender_customer, numberPhone_customer, bornDate_customer);

                                    /* Funcion Ajax para hacer la insercion a la DB */
                                    Ext.Ajax.request({
                                        url: "assets/php/backend.php",
                                        method: "POST",
                                        params: {
                                            funtionExecute: '4', // Funcion 1
                                            nameCustomer: name_customer,
                                            genderCustomer: gender_customer,
                                            numberPhoneCustomer: numberPhone_customer,
                                            bornDateCustomer: bornDate_customer
                                        },
                                        scope: this,
                                        success: function (response, request) {

                                            //console.log(response);

                                            Ext.Msg.show({
                                                title: 'Atencion',
                                                msg: 'Cliente registrado',
                                                buttons: Ext.Msg.OK,// OK - NO - YES - OKCANCEL - YESNO - YESNOCANCEL
                                                icon: Ext.Msg.INFO, //INFO - WARNING - QUESTION - ERROR
                                                width: 350, height: 130
                                            });

                                            modal.close();
                                            CustomerStore.load({
                                                params:
                                                {   /* Para enviar parametros a la funcion ajax que executa el Store
                                                            // Recuperamos valor de los inputs date con estableciendo el formato. de yyy-MM-dd
                                                            // fechai: Ext.Date.format(txt_fecha_inicio.getValue(), 'Y-m-d'), 
                                                            */
                                                    funtionExecute: '1' // Funcion 1
                                                }, callback: function (records, operation, success) {
                                                    // Recargamos la informacion de la consulta en el GRID Reporte para desplegar los resultados                                  
                                                    Ext.getCmp("dataCustomers").getView().refresh();
                                                    Ext.getCmp("dataCustomers").getView().refresh();
                                                },
                                                scope: this
                                            });
                                        },
                                        //En caso de que falle debemos indicar que hara
                                        failure: function (response, request) {

                                            Ext.Msg.show({
                                                title: 'Atencion',
                                                msg: 'No fue posible registrar al cliente',
                                                buttons: Ext.Msg.OK,// OK - NO - YES - OKCANCEL - YESNO - YESNOCANCEL
                                                icon: Ext.Msg.ERROR, //INFO - WARNING - QUESTION - ERROR
                                                width: 350, height: 130
                                            });

                                            modal.close();
                                            CustomerStore.load({
                                                params:
                                                {
                                                    funtionExecute: '1' // Funcion 1
                                                }, callback: function (records, operation, success) {
                                                    // Recargamos la informacion de la consulta en el GRID Reporte para desplegar los resultados                                  
                                                    Ext.getCmp("dataCustomers").getView().refresh();
                                                    Ext.getCmp("dataCustomers").getView().refresh();
                                                },
                                                scope: this
                                            });
                                            //console.log(response);
                                        }
                                    });


                                }
                            }
                        ]
                }
            ]
        }
    }).show();
}

function openEditCustomerModal(id_c, name_c, born_c, gender_c, phone_c) {

    //Recibo variables
    let e_id = id_c;
    let e_name = name_c;
    let e_born = born_c
    let e_gender = gender_c;
    let e_phone = phone_c;

    var modalEdit = Ext.create('Ext.window.Window', {
        title: 'Editar Cliente',
        width: 400,
        height: 300,
        modal: true,
        layout: 'fit',
        items:
        {
            //https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.layout.container.Form
            items: [
                {
                    layout: 'form',
                    width: 390,
                    height: 260,
                    bodyPadding: 25,
                    defaultType: 'textfield',
                    items: [

                        //Textfield oculto
                        {
                            xtype: 'hiddenfield',
                            id: 'id_customer_edit',
                            name: 'id_customer_edit',
                            value: e_id
                        },
                        {
                            //Textbox (Input Text)
                            fieldLabel: 'Nombre:',
                            id: 'name_customer_edit', //asigna un id asi como con JQUERY
                            name: 'name_customer_edit',
                            value: e_name, // Asignamos valor recibido de la consulta
                            style: "margin-top: 10px !important;",
                            allowBlank: false
                        },
                        {
                            //Combobox (Select)
                            fieldLabel: 'Genero',
                            xtype: 'combobox',
                            value: e_gender,
                            id: 'gender_customer_edit',
                            name: 'gender_customer_edit',
                            store: gender,
                            queryMode: 'local',
                            valueField: 'key',
                            // Para poder rendirizar los valores del store en el input SELECT 
                            tpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">',
                                '<div class="x-boundlist-item">{key} - {label}</div>',
                                '</tpl>'
                            ),
                            // template for the content inside text field
                            displayTpl: Ext.create('Ext.XTemplate',
                                '<tpl for=".">', '{key} - {label}', '</tpl>')
                        },
                        {
                            fieldLabel: 'Telefono:',
                            id: 'numberPhone_edit',
                            name: 'numberPhone_edit',
                            value: e_phone,
                            style: "margin-top: 10px !important;",
                            allowBlank: false,
                            // items: [text_search]

                        },
                        {
                            //Datetimepicker (Date)
                            xtype: 'datefield',
                            fieldLabel: 'Nacimiento:',
                            id: 'bornDate_customer_edit',
                            name: 'bornDate_customer_edit',
                            value: e_born,
                            style: "margin-top: 10px !important;",
                            format: 'Y/m/d',
                            submitFormat: 'Y-m-dH:m:s',
                            allowBlank: false
                        },
                    ],
                    buttons:
                        [
                            {
                                text: 'Cancelar',
                                bodyPadding: 25,
                                iconCls: 'icon-cancel',
                                handler: function () {
                                    //Limpiado el formulario de manera manual
                                    Ext.getCmp('name_customer_edit').setValue('');
                                    Ext.getCmp('gender_customer_edit').setValue('');
                                    Ext.getCmp('numberPhone_edit').setValue('');
                                    Ext.getCmp('bornDate_customer_edit').setValue('');
                                    modalEdit.close();
                                }
                            },
                            {
                                text: 'Guardar',
                                iconCls: 'icon-save',
                                formBind: true, //only enabled once the form is valid
                                disabled: false,
                                handler: function () {
                                    //Recuperamos los valores de los inputsy los asignamos a variables
                                    let id_customer_edit = Ext.getCmp('id_customer_edit').getValue();
                                    let name_customer = Ext.getCmp('name_customer_edit').getValue(); // Rucupera valor por id
                                    //Validamos que no este vacio el input
                                    if (name_customer == "") {
                                        //https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.window.MessageBox
                                        Ext.Msg.show({
                                            title: 'Atención.',
                                            msg: 'Ingresa el nombre del cliente, es requerido.',
                                            buttons: Ext.Msg.OK,// OK - NO - YES - OKCANCEL - YESNO - YESNOCANCEL
                                            icon: Ext.Msg.WARNING, //INFO - WARNING - QUESTION - ERROR
                                            width: 350, height: 130
                                        });
                                        Ext.getCmp('name_customer_edit').focus(); // Devolvemos el foco al campo.
                                        return;
                                    }

                                    let gender_customer = Ext.getCmp('gender_customer_edit').getValue();
                                    if (gender_customer == "" || gender_customer == null) {
                                        Ext.Msg.show({
                                            title: 'Atención.',
                                            msg: 'Selecciona el genero del cliente, es requerido.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.WARNING,
                                            width: 350, height: 130
                                        });
                                        Ext.getCmp('gender_customer_edit').focus();
                                        return;
                                    }

                                    let numberPhone_customer = Ext.getCmp('numberPhone_edit').getValue();
                                    if (numberPhone_customer == "") {
                                        Ext.Msg.show({
                                            title: 'Atención.',
                                            msg: 'Ingresa el no. de telefonico del cliente, es requerido.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.WARNING,
                                            width: 350, height: 130
                                        });
                                        Ext.getCmp('numberPhone_edit').focus();
                                        return;
                                    }

                                    let bornDate_customer = Ext.Date.format(Ext.getCmp('bornDate_customer_edit').getValue(), 'Y-m-d'); // Tratamiento a input DATE
                                    if (bornDate_customer == "") {
                                        Ext.Msg.show({
                                            title: 'Atención.',
                                            msg: 'Selecciona la fecha de nacimiento del cliente, es requerido.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.WARNING,
                                            width: 350, height: 130
                                        });
                                        Ext.getCmp('bornDate_customer_edit').focus();
                                        return;
                                    }
                                    //console.log(id_customer_edit, name_customer, gender_customer, numberPhone_customer, bornDate_customer);

                                    
                                    /* Funcion Ajax para hacer la insercion a la DB */
                                    Ext.Ajax.request({
                                        url: "assets/php/backend.php",
                                        method: "POST",
                                        params: {
                                            funtionExecute: '5', // Funcion 1
                                            id: id_customer_edit,
                                            nameCustomer: name_customer,
                                            genderCustomer: gender_customer,
                                            numberPhoneCustomer: numberPhone_customer,
                                            bornDateCustomer: bornDate_customer                                           
                                        },
                                        scope: this,
                                        success: function (response, request) {
                                            //console.log(response);

                                            Ext.Msg.show({
                                                title: 'Atencion',
                                                msg: 'Datos Actualizados',
                                                buttons: Ext.Msg.OK,// OK - NO - YES - OKCANCEL - YESNO - YESNOCANCEL
                                                icon: Ext.Msg.INFO, //INFO - WARNING - QUESTION - ERROR
                                                width: 350, height: 130
                                            });

                                            modalEdit.close();
                                            CustomerStore.load({
                                                params:
                                                {  
                                                    funtionExecute: '1' // Funcion 1
                                                }, callback: function (records, operation, success) {
                                                    // Recargamos la informacion de la consulta en el GRID Reporte para desplegar los resultados                                  
                                                    Ext.getCmp("dataCustomers").getView().refresh();
                                                    Ext.getCmp("dataCustomers").getView().refresh();
                                                },
                                                scope: this
                                            });
                                        },
                                        //En caso de que falle debemos indicar que hara
                                        failure: function (response, request) {

                                            Ext.Msg.show({
                                                title: 'Atencion',
                                                msg: 'No fue posible actualizar los datos',
                                                buttons: Ext.Msg.OK,// OK - NO - YES - OKCANCEL - YESNO - YESNOCANCEL
                                                icon: Ext.Msg.ERROR, //INFO - WARNING - QUESTION - ERROR
                                                width: 350, height: 130
                                            });

                                            modal.close();
                                            CustomerStore.load({
                                                params:
                                                {
                                                    funtionExecute: '1' // Funcion 1
                                                }, callback: function (records, operation, success) {
                                                    // Recargamos la informacion de la consulta en el GRID Reporte para desplegar los resultados                                  
                                                    Ext.getCmp("dataCustomers").getView().refresh();
                                                    Ext.getCmp("dataCustomers").getView().refresh();
                                                },
                                                scope: this
                                            });
                                            //console.log(response);
                                        }
                                    });
                                    

                                }
                            }
                        ]
                }
            ]
        }
    }).show();
}
//Declaramos los inputs que estaran presentes en nuestro Ejemplo, SI LLEGARAN A SER NECESARIOS
Ext.onReady(function () {

    //Panel Principal de nuestra aplicacion
    //https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.panel.Panel 
    Ext.create('Ext.panel.Panel', {
        title: 'Admin. Clientes Ver 1.0.0', // Titulo del panel
        width: 650, // Ancho
        height: 400, // Alto
        renderTo: 'main',
        frame: true,
        layout: {
            type: 'vbox',       // Arrange child items vertically
            align: 'stretch',    // Each takes up full width
            padding: 10
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                items: [
                    {
                        text: 'Nuevo cliente',
                        id: 'button_newCustomer',
                        iconCls: 'icon-plus',
                        handler: function () {
                            //Abrimos Moddal llamando a ejecutar la funcion
                            openCustomerModal();
                        },
                    },
                    {
                        //Agregando input en el toolbar para hacer un buscador 
                        //https://docs.sencha.com/extjs/4.1.1/#!/api/Ext.form.field.Base-method-getValue
                        xtype: 'textfield',
                        inputType: 'search',
                        border: false,
                        width: 250,
                        id: 'word',
                        emptyText: 'Escribe un nombre y pulsa enter...', // Para hacer placeholder de HTML con EXTJS
                        enableKeyEvents: true,
                        disabled: false, //true       
                        allowBlank: false, // Campo requerido
                        initComponent: function () {
                            this.callParent();
                            this.on('specialkey', this.checkEnterKey, this);
                        },
                        // Handle enter key presses, execute the search if the field has a value
                        checkEnterKey: function (field, e) {
                            var value = this.getValue();

                            if (e.getKey() === e.ENTER && !Ext.isEmpty(value)) {
                                CustomerStore.load({
                                    params:
                                    {
                                        funtionExecute: '2',// Funcion 2: readRecord en PHP 
                                        word: value
                                    }, callback: function (records, operation, success) {
                                        // Recargamos la informacion de la consulta en el GRID Reporte para desplegar los resultados                                  
                                        Ext.getCmp("dataCustomers").getView().refresh();
                                        //Ext.getCmp("dataCustomers").getView().refresh();
                                    },
                                    scope: this
                                });
                            }
                        }
                    },
                    {
                        iconCls: 'icon-load',
                        handler: function () {
                            //Mandamos a Ejecutar el CustomerStore para que muestre los datos en el GRID tabla
                            CustomerStore.load({
                                params:
                                {   /* Para enviar parametros a la funcion ajax que executa el Store
                                        // Recuperamos valor de los inputs date con estableciendo el formato. de yyy-MM-dd
                                        // fechai: Ext.Date.format(txt_fecha_inicio.getValue(), 'Y-m-d'), 
                                        */
                                    funtionExecute: '1' // Funcion 1
                                }, callback: function (records, operation, success) {
                                    // Recargamos la informacion de la consulta en el GRID Reporte para desplegar los resultados                                  
                                    Ext.getCmp("dataCustomers").getView().refresh();
                                    // Ext.getCmp("dataCustomers").getView().refresh();
                                },
                                scope: this
                            });
                        }
                    }
                ]
            },
            {
                xtype: 'grid', // Creamos una GRID o tabla en el panel
                id: 'dataCustomers',
                store: CustomerStore,//Definimos el Store que tendra asociado
                //Definimos el nombre de las columnas que se desplegaran en nuestra GRID o tabla
                viewConfig: {
                    scrollOffset: 0,
                    emptyText: 'No se encontraron clientes...'
                },
                // forceFit: true,
                flex: 1, // Use 1/3 of Container's height (hint to Box layout)
                columns: [
                    {

                        header: 'ID',
                        autoSizeColumn: true, // Si puede cambiar el ancho de la columna
                        width: 45, // Se define ancho de la columna
                        align: 'center', //Alineacion del cotenido
                        style: "margin-top: 5px !important;",
                        dataIndex: 'id', // Para insertar los datos almacenados en el Store
                    },
                    {
                        header: 'Nombre',
                        autoSizeColumn: true,
                        //align: 'left',
                        style: "margin-top: 5px !important;",
                        dataIndex: 'nombre',
                    },
                    {
                        header: 'Genero',
                        autoSizeColumn: true,
                        align: 'center',
                        style: "margin-top: 5px !important;",
                        dataIndex: 'sexo',
                    },
                    {
                        header: 'Telefono',
                        autoSizeColumn: true,
                        align: 'center',
                        style: "margin-top: 5px !important;",
                        dataIndex: 'telefono',
                    },
                    {
                        header: 'Nacimiento',
                        autoSizeColumn: true,
                        align: 'center',
                        style: "margin-top: 5px !important;",
                        dataIndex: 'nacimiento',
                    },
                    //Botones con Acciones Editar/Eliminar 
                    {
                        header: 'Editar',
                        xtype: 'actioncolumn',
                        flex: 0.25,
                        //autoSizeColumn: true,
                        style: "margin-top: 5px !important;",
                        align: 'center',
                        items: [
                            {
                                icon: 'assets/imgs/icos/16px-edit-1.png',
                                handler: function (grid, rowIndex, colIndex) {
                                    var idPerson = grid.getStore().getAt(rowIndex);
                                    let id = idPerson.get('id');
                                    CustomerStore.load({
                                        params:
                                        {
                                            funtionExecute: '3',// Funcion 2: readRecord en PHP 
                                            id: id
                                        }, callback: function (records, operation, success) {
                                            //Tomando los datos para abrir modal con sobrecarga de informacion.
                                            //console.log(records);
                                            let id_c = records[0].data.id; let name_c = records[0].data.nombre;
                                            let born_c = records[0].data.nacimiento; let gender_c = records[0].data.sexo;
                                            let phone_c = records[0].data.telefono;
                                            openEditCustomerModal(id_c, name_c, born_c, gender_c, phone_c);
                                        },
                                        scope: this
                                    });
                                }
                            }
                        ]
                    },
                    {
                        header: 'Eliminar',
                        xtype: 'actioncolumn',
                        flex: 0.25,
                        //autoSizeColumn: true,
                        style: "margin-top: 5px !important;",
                        align: 'center',
                        items: [
                            {
                                icon: 'assets/imgs/icos/16px-delete-1.png',
                                text: 'Ver PDF',
                                handler: function (grid, rowIndex, colIndex) {

                                    //Se puede agregar un Message Box de confirmacion de eliminacion para realizar la operacion.
                                    var idPerson = grid.getStore().getAt(rowIndex);
                                    let id = idPerson.get('id');
                                    CustomerStore.load({
                                        params:
                                        {
                                            funtionExecute: '6', // Funcion 1
                                            id: id
                                        }, callback: function (records, operation, success) {
                                            //Ejecutamos una nueva busqueda para recargar la informacion almacenada en la DB y mostrarla en GRID, 
                                            //dar la impresion de tiempo real
                                            CustomerStore.load({
                                                params:
                                                {
                                                    funtionExecute: '1'
                                                }, callback: function (records, operation, success) {

                                                    Ext.getCmp("dataCustomers").getView().refresh();
                                                },
                                                scope: this
                                            });

                                        },
                                        scope: this
                                    });

                                }
                            }
                        ]
                    },

                ],

            }]
    });
});